import { mount } from "@vue/test-utils";
import Card from "@/components/Card.vue";

/* global test, expect */

test("Card", () => {
  // render component
  // a title without letters should not be rendered
  const card = mount(Card, {
    propsData: {
      title: "       "
    }
  });
  expect(card.find(".card-title").exists()).toBe(false);

  // a title with letters should be rendered
  const cardTitled = mount(Card, {
    propsData: {
      title: "Title"
    }
  });
  expect(cardTitled.find(".card-title").exists()).toBe(true);
});
