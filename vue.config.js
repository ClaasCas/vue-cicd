/* global require */
const path = require("path");

/* global module, __dirname */
module.exports = {
  outputDir: path.resolve(__dirname, "./public"),
  assetsDir: "./static"
};
